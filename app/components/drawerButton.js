import React from 'react'
import {TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import PropTypes from 'prop-types'

const DrawerButton = ({ navigation }) => (
  <TouchableOpacity
  onPress={() => navigation.openDrawer()}
      style={{padding: 10}}>
    <Icon
      name="ios-menu"
      size={25}
      color="white"
    />
  </TouchableOpacity>
)

DrawerButton.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default DrawerButton
