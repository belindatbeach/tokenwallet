import React from 'react';
import { View, StyleSheet, Text, TouchableWithoutFeedback, Opacity } from 'react-native';
import { MaterialIcons, MaterialCommunityIcons, Entypo, Ionicons } from '@expo/vector-icons';
import { LinearGradient, } from 'expo';

import { NavigationActions } from 'react-navigation';
import ResetNavigation from './../../app/util/resetNavigation'

class Footer extends React.Component {
    state = {
        activeScreen: 'Home',
        pressedScreen: false
    }

    screens = {    
        Home: {
            icon: 'wallet',
            iconType: Entypo,
            size: 29,
            modes: [ 'hot', 'cold' ]
        },    
        Send: {
            icon: 'checkbox-multiple-blank-circle-outline',
            iconType: MaterialCommunityIcons,
            size: 28,
            modes: [ 'hot' ]
        },
        Receive: {
            icon: 'checkbox-multiple-marked',
            iconType: MaterialCommunityIcons,
            size: 29,
            modes: [ 'hot' ]
        },
        Accounts: {
            icon: 'lock',
            iconType: MaterialCommunityIcons,
            size: 28,
            modes: [ 'cold' ]
        },
        GetVerified: {
            icon: 'md-contact',
            iconType: Ionicons,
            size: 29,
            modes: [ 'hot', 'cold' ]
        },
        Settings: {
            icon: 'cog',
            iconType: Entypo,
            size: 28,
            modes: [ 'hot', 'cold' ],
        }
    }

    constructor(props) {
        super(props);

        this.handlePressIn = this.handlePressIn.bind(this);
        this.handlePress = this.handlePress.bind(this);
        this.getTabBackground = this.getTabBackground.bind(this);
        this.getTabColour = this.getTabColour.bind(this);
        this.mounted = this.mounted.bind(this);
    }

    handlePressIn(screenName) {
        this.setState({ pressedScreen: screenName });
    }

    handlePress(screenName) {
        const {navigate} = this.props.navigation;

        navigate(screenName)
        // ResetNavigation.dispatchToSingleRoute(this.props.navigation, screenName)
        console.log(screenName);
        // const { routes, index } = this.props.navigation.state;
        // const currentRoute = routes[index];

        // if(screenName == this.state.activeScreen)
        //     return;

        // this.props.navigation.dispatch(NavigationActions.navigate({ routeName: screenName }));
     

        this.setState({ 
            pressedScreen: false, 
            activeScreen: screenName 
        });        
    }

    getTabBackground(screenName) {
        if(this.state.activeScreen == screenName)
            return [ '#ED3F21', '#DC2C73' ];

        // Return two transparent colours as android requires two colours minimum
        return [ 'rgba(0, 0, 0, 0)', 'rgba(0, 0, 0, 0)' ];
    }

    getTabColour(screenName) {
        if(this.state.activeScreen == screenName)
            return '#ffffff';

        return '#eeeeee';
    }

    // componentDidUpdate() {
    //     const { routes, index } = this.props.navigation.state;
    //     const currentRoute = routes[index];

    //     if(currentRoute.routeName !== this.state.activeScreen)
    //         this.setState({ activeScreen: currentRoute.routeName });
    // }

    mounted({ nativeEvent }) {
        const { height } = nativeEvent.layout;
        this.props.broadcastFooterHeight(height);
    }

    render() {
        const filtered = Object.entries(this.screens).filter(([ screenName, { modes } ]) => {
            return modes;
        });

        const tabs = filtered.map(([ screenName, screen ]) => {
            const { icon: iconName, iconType: IconType, size, modes } = screen;

            const style = {};

            if(size) {
                style.height = size;
                style.width = size;
            }

            const icon = <IconType 
                name={ iconName } 
                color={ this.getTabColour(screenName) } 
                style={[ styles.footerElementIcon, style ]} 
            />;

            return (
                <TouchableWithoutFeedback key={ screenName } onPressIn={ () => this.handlePressIn(screenName) } onPress={ () => this.handlePress(screenName) }>
                    <View style={ styles.footerElement }>
                        <LinearGradient start={[ 1, 0 ]} end={[ 0, 0 ]} colors={ this.getTabBackground(screenName) } style={ styles.footerElementBackground }>
                            { icon }
                        </LinearGradient>
                        <Text style={ styles.footerElementText }>{ screenName }</Text>
                    </View>
                </TouchableWithoutFeedback>                            
            )
        });

        return (
            <LinearGradient colors={[ 'rgba(25, 32, 40, 0)', '#31353E' ]} style={ styles.footer }>
                <View style={ styles.footerContainer }>
                    { tabs } 
                </View>               
            </LinearGradient>     
        )
    }
};

const styles = StyleSheet.create({
    footer: {
        width: '100%',
        position: 'absolute',
        left: 0,
        bottom: 0,        
    },
    footerContainer: {
        display: 'flex',
        flexDirection: 'row',
        padding: 15,
        paddingBottom: 15
    },
    footerElement: {
        flex: 1,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    footerElementBackground: {
        padding: 8,
        marginBottom: 4,
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center'
    },
    footerElementIcon: {
        fontSize: 30,
        lineHeight: 30,
        textAlign: 'center'
    },
    footerElementText: {
        fontSize: 13, 
        color: '#eeeeee'
    }
});

export default Footer;