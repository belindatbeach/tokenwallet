import {NavigationActions, StackActions} from 'react-navigation'
const resetNavigation = {
    dispatchToSingleRoute: (navigation, routeName) => {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({routeName}),
            ],
        })
        navigation.dispatch(resetAction)
    },

    dispatchToDrawerRoute: (navigation, drawerRoute) => {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    routeName: 'Home',
                    params: {},
                    action: NavigationActions.navigate({routeName: drawerRoute}),
                }),
            ],
        })
        navigation.dispatch(resetAction)
    },

    dispatchUnderDrawer: (navigation, drawerRoute, finalRoute) => {
        if (drawerRoute === "GetVerified") {
            const resetAction = StackActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({
                        routeName: 'Home',
                        params: {},
                        action: NavigationActions.navigate({routeName: drawerRoute}),
                    }),
                    //NavigationActions.navigate({routeName: finalRoute}),
                ],
            })
            navigation.dispatch(resetAction)
        } else {
            const resetAction = StackActions.reset({
                index: 1,
                actions: [
                    NavigationActions.navigate({
                        routeName: 'Home',
                        params: {},
                        action: NavigationActions.navigate({routeName: drawerRoute}),
                    }),
                    NavigationActions.navigate({routeName: finalRoute}),
                ],
            })
            navigation.dispatch(resetAction)
        }
    },
    dispatchUnderTwoFactor: (navigation) => {
        const resetAction = StackActions.reset({
            index: 2,
            actions: [
                NavigationActions.navigate({
                    routeName: 'Home',
                    action: NavigationActions.navigate({routeName: 'Settings'}),
                }),
                NavigationActions.navigate({routeName: 'SettingsSecurity'}),
                NavigationActions.navigate({routeName: 'TwoFactor'})
            ],
        })
        navigation.dispatch(resetAction)
    },

    dispatchUnderDrawerWithParams: (navigation, drawerRoute, finalRoute, params) => {
        const resetAction = StackActions.reset({
            index: 1,
            actions: [
                NavigationActions.navigate({
                    routeName: 'Home',
                    params: {},
                    action: NavigationActions.navigate({routeName: drawerRoute}),
                }),
                NavigationActions.navigate({
                    routeName: finalRoute,
                    params,
                }),
            ],
        })
        navigation.dispatch(resetAction)
    },
}

export default resetNavigation
