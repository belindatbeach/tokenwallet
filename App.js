import React from 'react';
import { AppRegistry, Platform, StatusBar, StyleSheet, View, AsyncStorage } from 'react-native';
import Navigator from './app/routes/stackNavigator';
import Footer from './app/components/Footer';

export default class App extends React.Component {
  
  state = {
    isLoadingComplete: false,
  };
  render() {
    
      return (
        <View style={styles.container}>
          {/* {Platform.OS === 'ios' && <StatusBar barStyle="default" />} */}
          <Navigator />
          {/* <Footer /> */}
        </View>
      );
   
      
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});



AppRegistry.registerComponent("App", () => App);